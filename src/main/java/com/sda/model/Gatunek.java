package com.sda.model;

public enum Gatunek {
    HORROR, DRAMAT, KOMEDIA, ROMANS, SCIENCE_FICTION, FANTASY, HISTORYCZNA, ENCYKLOPEDIA;
}
