package com.sda.model;

public class Uzytkownik {
    private String Imie;
    private String Nazwisko;
    private String Pesel;
    private  Plec plec;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public int getId() {
        return id;
    }

    public String getPesel() {
        return Pesel;
    }

    public void setPesel(String pesel) {
        Pesel = pesel;
    }

    @Override
    public String toString() {
        return "Uzytkownik{" +
                "Imie='" + Imie + '\'' +
                ", Nazwisko='" + Nazwisko + '\'' +
                '}';
    }

    public Uzytkownik(String imie, String nazwisko) {
        Imie = imie;
        Nazwisko = nazwisko;
    }

    public String getImie() {

        return Imie;
    }

    public void setImie(String imie) {
        Imie = imie;
    }

    public String getNazwisko() {
        return Nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        Nazwisko = nazwisko;
    }
}
